// control dice two dice each for two people
// control scoring level
// display hit on scoring level
// display text for each hit
// display win/lose

const actionText = document.querySelector(".action-text")
const playerScoreLevel = document.querySelector(".player .score-level-pointer")
const playerScoreText = document.querySelector(".player .score-text")
const monsterScoreLevel = document.querySelector(
  ".monster .score-level-pointer"
)
const monsterScoreText = document.querySelector(".monster .score-text")
const playerDice1 = document.querySelector(".player .dice-1")
const playerDice2 = document.querySelector(".player .dice-2")
const monsterDice1 = document.querySelector(".monster .dice-1")
const monsterDice2 = document.querySelector(".monster .dice-2")
const actionButton = document.querySelector(".action-btn")
const actionButtonReset = document.querySelector(".action-btn--reset")

const getDiceRoll = () => {
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
  return Math.floor(Math.random() * 6) + 1
}

let player
let monster

const setupGame = () => {
  player = 100
  monster = 100

  actionButtonReset.style.display = "none"
  actionButton.style.display = "block"

  playerDice1.setAttribute("src", "./images/dice-1.svg")
  playerDice2.setAttribute("src", "./images/dice-1.svg")
  monsterDice1.setAttribute("src", "./images/dice-1.svg")
  monsterDice2.setAttribute("src", "./images/dice-1.svg")

  playerScoreLevel.style.height = `${player}%`
  monsterScoreLevel.style.height = `${monster}%`
  playerScoreText.innerHTML = player
  monsterScoreText.innerHTML = monster

  actionText.innerHTML = "Press Attack to start"
  actionText.style.color = "black"
}

const onAttack = () => {
  let playerDice1Count = getDiceRoll()
  let playerDice2Count = getDiceRoll()
  let monsterDice1Count = getDiceRoll()
  let monsterDice2Count = getDiceRoll()
  let playerCount = playerDice1Count + playerDice2Count
  let monsterCount = monsterDice1Count + monsterDice2Count
  let reduction = 0
  let gameOver = false
  let text = "No damage :')"
  if (playerCount > monsterCount) {
    reduction = playerCount - monsterCount
    monster -= reduction
    text = `You hit for ${reduction} \\o/`
  } else if (playerCount < monsterCount) {
    reduction = monsterCount - playerCount
    player -= reduction
    text = `You got hit for ${reduction} :(`
  }

  if (player <= 0) {
    text = "Game Over :("
    actionText.style.color = "red"
    player = 0
    gameOver = true
  }
  if (monster <= 0) {
    text = "You Win \\o/"
    actionText.style.color = "green"
    monster = 0
    gameOver = true
  }

  if (gameOver) {
    actionButtonReset.style.display = "block"
    actionButton.style.display = "none"
  }

  // Update score level
  playerScoreLevel.style.height = `${player}%`
  monsterScoreLevel.style.height = `${monster}%`
  playerScoreText.innerHTML = player
  monsterScoreText.innerHTML = monster

  // Show main text
  actionText.innerHTML = text

  // Update dice UI
  playerDice1.setAttribute("src", `./images/dice-${playerDice1Count}.svg`)
  playerDice2.setAttribute("src", `./images/dice-${playerDice2Count}.svg`)
  monsterDice1.setAttribute("src", `./images/dice-${monsterDice1Count}.svg`)
  monsterDice2.setAttribute("src", `./images/dice-${monsterDice2Count}.svg`)
}

actionButton.addEventListener("click", onAttack)
actionButtonReset.addEventListener("click", () => {
  const result = window.confirm("Do you want to restart the game?")
  if (result) {
    setupGame()
  }
})

setupGame()
